program Project1;

{$APPTYPE CONSOLE}

uses
  Winapi.Windows,
  Unit_SSL in 'Unit_SSL.pas';

Const
  pSend :PAnsiChar = 'GET / HTTP/1.1' + #13#10 +
  'Host: github.com' + #13#10 +
  'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko' + #13#10 +
  'Accept:*/*' + #13#10#13#10;
Var
  lpSSL  :TSSL_Socket;
  Buffer :PAnsiChar;
begin
  lpSSL := TSSL_Socket.Create;
  lpSSL.UseSSL := True;
  lpSSL.Connect('github.com', 443);
  if lpSSL.Connected then
  begin
    If lpSSL.Send(pSend, lstrlenA(pSend)) > 0 Then
    begin
      Buffer := GetMemory(1024 * 1024 * 64);
      lpSSL.Recv(Buffer, 1024 * 1024 * 64);
      Writeln(Buffer);
      FreeMemory(Buffer);
    end;
    lpSSL.Disconnect;
  end;
  lpSSL.Free;
  Readln;
end.
